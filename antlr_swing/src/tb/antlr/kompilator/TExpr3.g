tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0;
}
prog    : ( b+=block | e+=expr | d+=decl)* -> main(expressions={$e},declarations={$d},blocks={$b});

decl  :
        ^(VAR i1=ID) {locals.newSymbol($ID.text);} -> dek(n={$ID.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}
    
block : ^(LCB {locals.enterScope();} ( b+=block | e+=expr | d+=decl)* {locals.leaveScope();}) 
              -> startBlock(expressions={$e},declarations={$d},blocks={$b});

expr    : ^(PLUS  e1=expr e2=expr) -> add(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> substract(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> multiply(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> divide(p1={$e1.st},p2={$e2.st})
        | ^(PODST i1=ID   e2=expr) -> setNumber(name={$ID.text},value={$e2.st})
        | ID                       -> getNumber(name={$ID.text})
        | INT  {numer++;}          -> int(i={$INT.text},j={numer.toString()})
        | ^(FUN t=TYPE id=ID p=params?)   -> function(type={$t.text},name={$id.text},parameters={$p.st})
        | ^(IF e1=expr cond=sign e2=expr e3=expr e4=elseaction?) {numer++;} 
            -> ifblock(left={$e1.st},right={$e2.st},operator={$cond.out},thenact={$e3.st}, elseact={$e4.st}, nr = {numer.toString()})
    ;
    
sign returns [String out]: myChar=OPERATOR
    { $out = getJumpLabelFromOperator($myChar.text);
    }
    ;
    
elseaction : expr -> elseAction(expression={$expr.st}) 
     ;
     
params : ^(PARAM e1+=param*) -> params(parameters={$e1})
      ;
      
param: ^(t=TYPE id=ID) -> singleParam(type={$t.text},name={$id.text})
      ;
    