tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog    :   (
    expr
    | printExpr
    | declaration
  )*
  ;

printExpr : PRINT expr {
             drukuj ($expr.text + " = " + $expr.out.toString());
            };   
            
declaration: ^(VAR ID) { newSymbol($ID.text); }
            ;    

expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = $e1.out + $e2.out;}
        | ^(MINUS e1=expr e2=expr) {$out = $e1.out - $e2.out;}
        | ^(MUL   e1=expr e2=expr) {$out = $e1.out * $e2.out;}
        | ^(DIV   e1=expr e2=expr) {$out = $e1.out / $e2.out;}
        | ^(SHIFTL   e1=expr e2=expr) {$out = $e1.out << $e2.out;}
        | ^(SHIFTR   e1=expr e2=expr) {$out = $e1.out >> $e2.out;}
        | ^(EXP   e1=expr e2=expr) {$out = (int)Math.pow($e1.out,$e2.out);}
        |^(PODST ID e1=expr) { setSymbol($ID.text,$e1.out); $out=getSymbol($ID.text);}
        | ID                        {$out = getSymbol($ID.text);}
        | INT                       {$out = getInt($INT.text);}
        ;
