package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;

import tb.antlr.symbolTable.GlobalSymbols;
import tb.antlr.symbolTable.LocalSymbols;

public class MyTreeParser extends TreeParser {
	
	private GlobalSymbols _globalSymbols;
	
    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	public boolean hasSymbol(String name) {
		return _globalSymbols.hasSymbol(name);
	}

	public void newSymbol(String name) throws RuntimeException{
		_globalSymbols.newSymbol(name);
	}
	
	public void setSymbol(String name, Integer value) throws RuntimeException {
		_globalSymbols.setSymbol(name, value);	
	}
	
	public Integer getSymbol(String name) throws RuntimeException {
		return _globalSymbols.getSymbol(name);
	}
	
}
