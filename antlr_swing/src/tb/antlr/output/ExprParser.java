// $ANTLR 3.5.1 C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g 2022-03-15 19:14:24

package tb.antlr;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

import org.antlr.runtime.debug.*;
import java.io.IOException;
import org.antlr.runtime.tree.*;


@SuppressWarnings("all")
public class ExprParser extends DebugParser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "COMMA", "DIV", "EXP", "FUN", 
		"ID", "INT", "LCB", "LP", "MINUS", "MUL", "NL", "PLUS", "PODST", "PRINT", 
		"RCB", "RP", "SHIFTL", "SHIFTR", "TYPE", "VAR", "WS"
	};
	public static final int EOF=-1;
	public static final int COMMA=4;
	public static final int DIV=5;
	public static final int EXP=6;
	public static final int FUN=7;
	public static final int ID=8;
	public static final int INT=9;
	public static final int LCB=10;
	public static final int LP=11;
	public static final int MINUS=12;
	public static final int MUL=13;
	public static final int NL=14;
	public static final int PLUS=15;
	public static final int PODST=16;
	public static final int PRINT=17;
	public static final int RCB=18;
	public static final int RP=19;
	public static final int SHIFTL=20;
	public static final int SHIFTR=21;
	public static final int TYPE=22;
	public static final int VAR=23;
	public static final int WS=24;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators


	public static final String[] ruleNames = new String[] {
		"invalidRule", "group", "function", "atom", "multExpr", "stat", "prog", 
		"params", "expr"
	};

	public static final boolean[] decisionCanBacktrack = new boolean[] {
		false, // invalid decision
		false, false, false, false, false, false, false, false
	};

 
	public int ruleLevel = 0;
	public int getRuleLevel() { return ruleLevel; }
	public void incRuleLevel() { ruleLevel++; }
	public void decRuleLevel() { ruleLevel--; }
	public ExprParser(TokenStream input) {
		this(input, DebugEventSocketProxy.DEFAULT_DEBUGGER_PORT, new RecognizerSharedState());
	}
	public ExprParser(TokenStream input, int port, RecognizerSharedState state) {
		super(input, state);
		DebugEventSocketProxy proxy =
			new DebugEventSocketProxy(this,port,adaptor);
		setDebugListener(proxy);
		setTokenStream(new DebugTokenStream(input,proxy));
		try {
			proxy.handshake();
		}
		catch (IOException ioe) {
			reportError(ioe);
		}
		TreeAdaptor adap = new CommonTreeAdaptor();
		setTreeAdaptor(adap);
		proxy.setTreeAdaptor(adap);
	}

	public ExprParser(TokenStream input, DebugEventListener dbg) {
		super(input, dbg);
		 
		TreeAdaptor adap = new CommonTreeAdaptor();
		setTreeAdaptor(adap);

	}

	protected boolean evalPredicate(boolean result, String predicate) {
		dbg.semanticPredicate(result, predicate);
		return result;
	}

		protected DebugTreeAdaptor adaptor;
		public void setTreeAdaptor(TreeAdaptor adaptor) {
			this.adaptor = new DebugTreeAdaptor(dbg,adaptor);
		}
		public TreeAdaptor getTreeAdaptor() {
			return adaptor;
		}
	@Override public String[] getTokenNames() { return ExprParser.tokenNames; }
	@Override public String getGrammarFileName() { return "C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g"; }


	public static class prog_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "prog"
	// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:16:1: prog : ( stat | group )+ EOF !;
	public final ExprParser.prog_return prog() throws RecognitionException {
		ExprParser.prog_return retval = new ExprParser.prog_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token EOF3=null;
		ParserRuleReturnScope stat1 =null;
		ParserRuleReturnScope group2 =null;

		CommonTree EOF3_tree=null;

		try { dbg.enterRule(getGrammarFileName(), "prog");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(16, 0);

		try {
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:17:5: ( ( stat | group )+ EOF !)
			dbg.enterAlt(1);

			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:17:7: ( stat | group )+ EOF !
			{
			root_0 = (CommonTree)adaptor.nil();


			dbg.location(17,7);
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:17:7: ( stat | group )+
			int cnt1=0;
			try { dbg.enterSubRule(1);

			loop1:
			while (true) {
				int alt1=3;
				try { dbg.enterDecision(1, decisionCanBacktrack[1]);

				int LA1_0 = input.LA(1);
				if ( ((LA1_0 >= ID && LA1_0 <= INT)||LA1_0==LP||LA1_0==NL||LA1_0==PRINT||(LA1_0 >= TYPE && LA1_0 <= VAR)) ) {
					alt1=1;
				}
				else if ( (LA1_0==LCB) ) {
					alt1=2;
				}

				} finally {dbg.exitDecision(1);}

				switch (alt1) {
				case 1 :
					dbg.enterAlt(1);

					// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:17:8: stat
					{
					dbg.location(17,8);
					pushFollow(FOLLOW_stat_in_prog49);
					stat1=stat();
					state._fsp--;

					adaptor.addChild(root_0, stat1.getTree());

					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:17:15: group
					{
					dbg.location(17,15);
					pushFollow(FOLLOW_group_in_prog53);
					group2=group();
					state._fsp--;

					adaptor.addChild(root_0, group2.getTree());

					}
					break;

				default :
					if ( cnt1 >= 1 ) break loop1;
					EarlyExitException eee = new EarlyExitException(1, input);
					dbg.recognitionException(eee);

					throw eee;
				}
				cnt1++;
			}
			} finally {dbg.exitSubRule(1);}
			dbg.location(17,26);
			EOF3=(Token)match(input,EOF,FOLLOW_EOF_in_prog57); 
			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		dbg.location(17, 26);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "prog");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

		return retval;
	}
	// $ANTLR end "prog"


	public static class group_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "group"
	// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:19:1: group : LCB ^ ( stat | group )* RCB !;
	public final ExprParser.group_return group() throws RecognitionException {
		ExprParser.group_return retval = new ExprParser.group_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token LCB4=null;
		Token RCB7=null;
		ParserRuleReturnScope stat5 =null;
		ParserRuleReturnScope group6 =null;

		CommonTree LCB4_tree=null;
		CommonTree RCB7_tree=null;

		try { dbg.enterRule(getGrammarFileName(), "group");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(19, 0);

		try {
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:20:3: ( LCB ^ ( stat | group )* RCB !)
			dbg.enterAlt(1);

			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:20:5: LCB ^ ( stat | group )* RCB !
			{
			root_0 = (CommonTree)adaptor.nil();


			dbg.location(20,8);
			LCB4=(Token)match(input,LCB,FOLLOW_LCB_in_group72); 
			LCB4_tree = (CommonTree)adaptor.create(LCB4);
			root_0 = (CommonTree)adaptor.becomeRoot(LCB4_tree, root_0);
			dbg.location(20,10);
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:20:10: ( stat | group )*
			try { dbg.enterSubRule(2);

			loop2:
			while (true) {
				int alt2=3;
				try { dbg.enterDecision(2, decisionCanBacktrack[2]);

				int LA2_0 = input.LA(1);
				if ( ((LA2_0 >= ID && LA2_0 <= INT)||LA2_0==LP||LA2_0==NL||LA2_0==PRINT||(LA2_0 >= TYPE && LA2_0 <= VAR)) ) {
					alt2=1;
				}
				else if ( (LA2_0==LCB) ) {
					alt2=2;
				}

				} finally {dbg.exitDecision(2);}

				switch (alt2) {
				case 1 :
					dbg.enterAlt(1);

					// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:20:11: stat
					{
					dbg.location(20,11);
					pushFollow(FOLLOW_stat_in_group76);
					stat5=stat();
					state._fsp--;

					adaptor.addChild(root_0, stat5.getTree());

					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:20:18: group
					{
					dbg.location(20,18);
					pushFollow(FOLLOW_group_in_group80);
					group6=group();
					state._fsp--;

					adaptor.addChild(root_0, group6.getTree());

					}
					break;

				default :
					break loop2;
				}
			}
			} finally {dbg.exitSubRule(2);}
			dbg.location(20,29);
			RCB7=(Token)match(input,RCB,FOLLOW_RCB_in_group84); 
			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		dbg.location(20, 29);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "group");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

		return retval;
	}
	// $ANTLR end "group"


	public static class stat_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "stat"
	// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:22:1: stat : ( expr NL -> expr | PRINT expr NL -> PRINT expr | VAR ID PODST expr NL -> ^( VAR ID ) ^( PODST ID expr ) | VAR ID NL -> ^( VAR ID ) | ID PODST expr NL -> ^( PODST ID expr ) | function NL -> ^( FUN function ) | NL ->);
	public final ExprParser.stat_return stat() throws RecognitionException {
		ExprParser.stat_return retval = new ExprParser.stat_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token NL9=null;
		Token PRINT10=null;
		Token NL12=null;
		Token VAR13=null;
		Token ID14=null;
		Token PODST15=null;
		Token NL17=null;
		Token VAR18=null;
		Token ID19=null;
		Token NL20=null;
		Token ID21=null;
		Token PODST22=null;
		Token NL24=null;
		Token NL26=null;
		Token NL27=null;
		ParserRuleReturnScope expr8 =null;
		ParserRuleReturnScope expr11 =null;
		ParserRuleReturnScope expr16 =null;
		ParserRuleReturnScope expr23 =null;
		ParserRuleReturnScope function25 =null;

		CommonTree NL9_tree=null;
		CommonTree PRINT10_tree=null;
		CommonTree NL12_tree=null;
		CommonTree VAR13_tree=null;
		CommonTree ID14_tree=null;
		CommonTree PODST15_tree=null;
		CommonTree NL17_tree=null;
		CommonTree VAR18_tree=null;
		CommonTree ID19_tree=null;
		CommonTree NL20_tree=null;
		CommonTree ID21_tree=null;
		CommonTree PODST22_tree=null;
		CommonTree NL24_tree=null;
		CommonTree NL26_tree=null;
		CommonTree NL27_tree=null;
		RewriteRuleTokenStream stream_PRINT=new RewriteRuleTokenStream(adaptor,"token PRINT");
		RewriteRuleTokenStream stream_VAR=new RewriteRuleTokenStream(adaptor,"token VAR");
		RewriteRuleTokenStream stream_PODST=new RewriteRuleTokenStream(adaptor,"token PODST");
		RewriteRuleTokenStream stream_ID=new RewriteRuleTokenStream(adaptor,"token ID");
		RewriteRuleTokenStream stream_NL=new RewriteRuleTokenStream(adaptor,"token NL");
		RewriteRuleSubtreeStream stream_function=new RewriteRuleSubtreeStream(adaptor,"rule function");
		RewriteRuleSubtreeStream stream_expr=new RewriteRuleSubtreeStream(adaptor,"rule expr");

		try { dbg.enterRule(getGrammarFileName(), "stat");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(22, 0);

		try {
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:23:5: ( expr NL -> expr | PRINT expr NL -> PRINT expr | VAR ID PODST expr NL -> ^( VAR ID ) ^( PODST ID expr ) | VAR ID NL -> ^( VAR ID ) | ID PODST expr NL -> ^( PODST ID expr ) | function NL -> ^( FUN function ) | NL ->)
			int alt3=7;
			try { dbg.enterDecision(3, decisionCanBacktrack[3]);

			switch ( input.LA(1) ) {
			case INT:
			case LP:
				{
				alt3=1;
				}
				break;
			case ID:
				{
				int LA3_2 = input.LA(2);
				if ( (LA3_2==PODST) ) {
					alt3=5;
				}
				else if ( ((LA3_2 >= DIV && LA3_2 <= EXP)||(LA3_2 >= MINUS && LA3_2 <= PLUS)||(LA3_2 >= SHIFTL && LA3_2 <= SHIFTR)) ) {
					alt3=1;
				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 3, 2, input);
						dbg.recognitionException(nvae);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case PRINT:
				{
				alt3=2;
				}
				break;
			case VAR:
				{
				int LA3_4 = input.LA(2);
				if ( (LA3_4==ID) ) {
					int LA3_8 = input.LA(3);
					if ( (LA3_8==PODST) ) {
						alt3=3;
					}
					else if ( (LA3_8==NL) ) {
						alt3=4;
					}

					else {
						int nvaeMark = input.mark();
						try {
							for (int nvaeConsume = 0; nvaeConsume < 3 - 1; nvaeConsume++) {
								input.consume();
							}
							NoViableAltException nvae =
								new NoViableAltException("", 3, 8, input);
							dbg.recognitionException(nvae);
							throw nvae;
						} finally {
							input.rewind(nvaeMark);
						}
					}

				}

				else {
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 3, 4, input);
						dbg.recognitionException(nvae);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}

				}
				break;
			case TYPE:
				{
				alt3=6;
				}
				break;
			case NL:
				{
				alt3=7;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 3, 0, input);
				dbg.recognitionException(nvae);
				throw nvae;
			}
			} finally {dbg.exitDecision(3);}

			switch (alt3) {
				case 1 :
					dbg.enterAlt(1);

					// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:23:7: expr NL
					{
					dbg.location(23,7);
					pushFollow(FOLLOW_expr_in_stat97);
					expr8=expr();
					state._fsp--;

					stream_expr.add(expr8.getTree());dbg.location(23,12);
					NL9=(Token)match(input,NL,FOLLOW_NL_in_stat99);  
					stream_NL.add(NL9);

					// AST REWRITE
					// elements: expr
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 23:15: -> expr
					{
						dbg.location(23,18);
						adaptor.addChild(root_0, stream_expr.nextTree());
					}


					retval.tree = root_0;

					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:24:7: PRINT expr NL
					{
					dbg.location(24,7);
					PRINT10=(Token)match(input,PRINT,FOLLOW_PRINT_in_stat111);  
					stream_PRINT.add(PRINT10);
					dbg.location(24,13);
					pushFollow(FOLLOW_expr_in_stat113);
					expr11=expr();
					state._fsp--;

					stream_expr.add(expr11.getTree());dbg.location(24,18);
					NL12=(Token)match(input,NL,FOLLOW_NL_in_stat115);  
					stream_NL.add(NL12);

					// AST REWRITE
					// elements: PRINT, expr
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 24:21: -> PRINT expr
					{
						dbg.location(24,24);
						adaptor.addChild(root_0, stream_PRINT.nextNode());dbg.location(24,30);
						adaptor.addChild(root_0, stream_expr.nextTree());
					}


					retval.tree = root_0;

					}
					break;
				case 3 :
					dbg.enterAlt(3);

					// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:25:7: VAR ID PODST expr NL
					{
					dbg.location(25,7);
					VAR13=(Token)match(input,VAR,FOLLOW_VAR_in_stat129);  
					stream_VAR.add(VAR13);
					dbg.location(25,11);
					ID14=(Token)match(input,ID,FOLLOW_ID_in_stat131);  
					stream_ID.add(ID14);
					dbg.location(25,14);
					PODST15=(Token)match(input,PODST,FOLLOW_PODST_in_stat133);  
					stream_PODST.add(PODST15);
					dbg.location(25,20);
					pushFollow(FOLLOW_expr_in_stat135);
					expr16=expr();
					state._fsp--;

					stream_expr.add(expr16.getTree());dbg.location(25,25);
					NL17=(Token)match(input,NL,FOLLOW_NL_in_stat137);  
					stream_NL.add(NL17);

					// AST REWRITE
					// elements: expr, ID, ID, VAR, PODST
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 25:28: -> ^( VAR ID ) ^( PODST ID expr )
					{
						dbg.location(25,31);
						// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:25:31: ^( VAR ID )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						dbg.location(25,33);
						root_1 = (CommonTree)adaptor.becomeRoot(stream_VAR.nextNode(), root_1);
						dbg.location(25,37);
						adaptor.addChild(root_1, stream_ID.nextNode());
						adaptor.addChild(root_0, root_1);
						}
						dbg.location(25,41);
						// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:25:41: ^( PODST ID expr )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						dbg.location(25,43);
						root_1 = (CommonTree)adaptor.becomeRoot(stream_PODST.nextNode(), root_1);
						dbg.location(25,49);
						adaptor.addChild(root_1, stream_ID.nextNode());dbg.location(25,52);
						adaptor.addChild(root_1, stream_expr.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 4 :
					dbg.enterAlt(4);

					// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:26:7: VAR ID NL
					{
					dbg.location(26,7);
					VAR18=(Token)match(input,VAR,FOLLOW_VAR_in_stat161);  
					stream_VAR.add(VAR18);
					dbg.location(26,11);
					ID19=(Token)match(input,ID,FOLLOW_ID_in_stat163);  
					stream_ID.add(ID19);
					dbg.location(26,14);
					NL20=(Token)match(input,NL,FOLLOW_NL_in_stat165);  
					stream_NL.add(NL20);

					// AST REWRITE
					// elements: VAR, ID
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 26:17: -> ^( VAR ID )
					{
						dbg.location(26,20);
						// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:26:20: ^( VAR ID )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						dbg.location(26,22);
						root_1 = (CommonTree)adaptor.becomeRoot(stream_VAR.nextNode(), root_1);
						dbg.location(26,26);
						adaptor.addChild(root_1, stream_ID.nextNode());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 5 :
					dbg.enterAlt(5);

					// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:27:7: ID PODST expr NL
					{
					dbg.location(27,7);
					ID21=(Token)match(input,ID,FOLLOW_ID_in_stat181);  
					stream_ID.add(ID21);
					dbg.location(27,10);
					PODST22=(Token)match(input,PODST,FOLLOW_PODST_in_stat183);  
					stream_PODST.add(PODST22);
					dbg.location(27,16);
					pushFollow(FOLLOW_expr_in_stat185);
					expr23=expr();
					state._fsp--;

					stream_expr.add(expr23.getTree());dbg.location(27,21);
					NL24=(Token)match(input,NL,FOLLOW_NL_in_stat187);  
					stream_NL.add(NL24);

					// AST REWRITE
					// elements: expr, ID, PODST
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 27:24: -> ^( PODST ID expr )
					{
						dbg.location(27,27);
						// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:27:27: ^( PODST ID expr )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						dbg.location(27,29);
						root_1 = (CommonTree)adaptor.becomeRoot(stream_PODST.nextNode(), root_1);
						dbg.location(27,35);
						adaptor.addChild(root_1, stream_ID.nextNode());dbg.location(27,38);
						adaptor.addChild(root_1, stream_expr.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 6 :
					dbg.enterAlt(6);

					// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:28:7: function NL
					{
					dbg.location(28,7);
					pushFollow(FOLLOW_function_in_stat205);
					function25=function();
					state._fsp--;

					stream_function.add(function25.getTree());dbg.location(28,16);
					NL26=(Token)match(input,NL,FOLLOW_NL_in_stat207);  
					stream_NL.add(NL26);

					// AST REWRITE
					// elements: function
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 28:19: -> ^( FUN function )
					{
						dbg.location(28,22);
						// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:28:22: ^( FUN function )
						{
						CommonTree root_1 = (CommonTree)adaptor.nil();
						dbg.location(28,24);
						root_1 = (CommonTree)adaptor.becomeRoot((CommonTree)adaptor.create(FUN, "FUN"), root_1);
						dbg.location(28,28);
						adaptor.addChild(root_1, stream_function.nextTree());
						adaptor.addChild(root_0, root_1);
						}

					}


					retval.tree = root_0;

					}
					break;
				case 7 :
					dbg.enterAlt(7);

					// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:29:7: NL
					{
					dbg.location(29,7);
					NL27=(Token)match(input,NL,FOLLOW_NL_in_stat223);  
					stream_NL.add(NL27);

					// AST REWRITE
					// elements: 
					// token labels: 
					// rule labels: retval
					// token list labels: 
					// rule list labels: 
					// wildcard labels: 
					retval.tree = root_0;
					RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.getTree():null);

					root_0 = (CommonTree)adaptor.nil();
					// 29:10: ->
					{
						dbg.location(30,5);
						root_0 = null;
					}


					retval.tree = root_0;

					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		dbg.location(30, 4);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "stat");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

		return retval;
	}
	// $ANTLR end "stat"


	public static class function_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "function"
	// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:32:1: function : TYPE ID LP ( params )? RP ;
	public final ExprParser.function_return function() throws RecognitionException {
		ExprParser.function_return retval = new ExprParser.function_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token TYPE28=null;
		Token ID29=null;
		Token LP30=null;
		Token RP32=null;
		ParserRuleReturnScope params31 =null;

		CommonTree TYPE28_tree=null;
		CommonTree ID29_tree=null;
		CommonTree LP30_tree=null;
		CommonTree RP32_tree=null;

		try { dbg.enterRule(getGrammarFileName(), "function");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(32, 0);

		try {
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:33:3: ( TYPE ID LP ( params )? RP )
			dbg.enterAlt(1);

			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:34:3: TYPE ID LP ( params )? RP
			{
			root_0 = (CommonTree)adaptor.nil();


			dbg.location(34,3);
			TYPE28=(Token)match(input,TYPE,FOLLOW_TYPE_in_function246); 
			TYPE28_tree = (CommonTree)adaptor.create(TYPE28);
			adaptor.addChild(root_0, TYPE28_tree);
			dbg.location(34,8);
			ID29=(Token)match(input,ID,FOLLOW_ID_in_function248); 
			ID29_tree = (CommonTree)adaptor.create(ID29);
			adaptor.addChild(root_0, ID29_tree);
			dbg.location(34,11);
			LP30=(Token)match(input,LP,FOLLOW_LP_in_function250); 
			LP30_tree = (CommonTree)adaptor.create(LP30);
			adaptor.addChild(root_0, LP30_tree);
			dbg.location(34,14);
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:34:14: ( params )?
			int alt4=2;
			try { dbg.enterSubRule(4);
			try { dbg.enterDecision(4, decisionCanBacktrack[4]);

			int LA4_0 = input.LA(1);
			if ( (LA4_0==TYPE) ) {
				alt4=1;
			}
			} finally {dbg.exitDecision(4);}

			switch (alt4) {
				case 1 :
					dbg.enterAlt(1);

					// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:34:14: params
					{
					dbg.location(34,14);
					pushFollow(FOLLOW_params_in_function252);
					params31=params();
					state._fsp--;

					adaptor.addChild(root_0, params31.getTree());

					}
					break;

			}
			} finally {dbg.exitSubRule(4);}
			dbg.location(34,22);
			RP32=(Token)match(input,RP,FOLLOW_RP_in_function255); 
			RP32_tree = (CommonTree)adaptor.create(RP32);
			adaptor.addChild(root_0, RP32_tree);

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		dbg.location(35, 2);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "function");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

		return retval;
	}
	// $ANTLR end "function"


	public static class params_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "params"
	// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:37:1: params : TYPE ID ( COMMA TYPE ID )* ;
	public final ExprParser.params_return params() throws RecognitionException {
		ExprParser.params_return retval = new ExprParser.params_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token TYPE33=null;
		Token ID34=null;
		Token COMMA35=null;
		Token TYPE36=null;
		Token ID37=null;

		CommonTree TYPE33_tree=null;
		CommonTree ID34_tree=null;
		CommonTree COMMA35_tree=null;
		CommonTree TYPE36_tree=null;
		CommonTree ID37_tree=null;

		try { dbg.enterRule(getGrammarFileName(), "params");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(37, 0);

		try {
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:38:3: ( TYPE ID ( COMMA TYPE ID )* )
			dbg.enterAlt(1);

			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:39:3: TYPE ID ( COMMA TYPE ID )*
			{
			root_0 = (CommonTree)adaptor.nil();


			dbg.location(39,3);
			TYPE33=(Token)match(input,TYPE,FOLLOW_TYPE_in_params271); 
			TYPE33_tree = (CommonTree)adaptor.create(TYPE33);
			adaptor.addChild(root_0, TYPE33_tree);
			dbg.location(39,8);
			ID34=(Token)match(input,ID,FOLLOW_ID_in_params273); 
			ID34_tree = (CommonTree)adaptor.create(ID34);
			adaptor.addChild(root_0, ID34_tree);
			dbg.location(39,11);
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:39:11: ( COMMA TYPE ID )*
			try { dbg.enterSubRule(5);

			loop5:
			while (true) {
				int alt5=2;
				try { dbg.enterDecision(5, decisionCanBacktrack[5]);

				int LA5_0 = input.LA(1);
				if ( (LA5_0==COMMA) ) {
					alt5=1;
				}

				} finally {dbg.exitDecision(5);}

				switch (alt5) {
				case 1 :
					dbg.enterAlt(1);

					// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:39:12: COMMA TYPE ID
					{
					dbg.location(39,12);
					COMMA35=(Token)match(input,COMMA,FOLLOW_COMMA_in_params276); 
					COMMA35_tree = (CommonTree)adaptor.create(COMMA35);
					adaptor.addChild(root_0, COMMA35_tree);
					dbg.location(39,18);
					TYPE36=(Token)match(input,TYPE,FOLLOW_TYPE_in_params278); 
					TYPE36_tree = (CommonTree)adaptor.create(TYPE36);
					adaptor.addChild(root_0, TYPE36_tree);
					dbg.location(39,23);
					ID37=(Token)match(input,ID,FOLLOW_ID_in_params280); 
					ID37_tree = (CommonTree)adaptor.create(ID37);
					adaptor.addChild(root_0, ID37_tree);

					}
					break;

				default :
					break loop5;
				}
			}
			} finally {dbg.exitSubRule(5);}

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		dbg.location(40, 2);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "params");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

		return retval;
	}
	// $ANTLR end "params"


	public static class expr_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "expr"
	// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:42:1: expr : multExpr ( PLUS ^ multExpr | MINUS ^ multExpr | SHIFTL ^ multExpr | SHIFTR ^ multExpr )* ;
	public final ExprParser.expr_return expr() throws RecognitionException {
		ExprParser.expr_return retval = new ExprParser.expr_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token PLUS39=null;
		Token MINUS41=null;
		Token SHIFTL43=null;
		Token SHIFTR45=null;
		ParserRuleReturnScope multExpr38 =null;
		ParserRuleReturnScope multExpr40 =null;
		ParserRuleReturnScope multExpr42 =null;
		ParserRuleReturnScope multExpr44 =null;
		ParserRuleReturnScope multExpr46 =null;

		CommonTree PLUS39_tree=null;
		CommonTree MINUS41_tree=null;
		CommonTree SHIFTL43_tree=null;
		CommonTree SHIFTR45_tree=null;

		try { dbg.enterRule(getGrammarFileName(), "expr");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(42, 0);

		try {
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:43:5: ( multExpr ( PLUS ^ multExpr | MINUS ^ multExpr | SHIFTL ^ multExpr | SHIFTR ^ multExpr )* )
			dbg.enterAlt(1);

			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:43:7: multExpr ( PLUS ^ multExpr | MINUS ^ multExpr | SHIFTL ^ multExpr | SHIFTR ^ multExpr )*
			{
			root_0 = (CommonTree)adaptor.nil();


			dbg.location(43,7);
			pushFollow(FOLLOW_multExpr_in_expr297);
			multExpr38=multExpr();
			state._fsp--;

			adaptor.addChild(root_0, multExpr38.getTree());
			dbg.location(44,7);
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:44:7: ( PLUS ^ multExpr | MINUS ^ multExpr | SHIFTL ^ multExpr | SHIFTR ^ multExpr )*
			try { dbg.enterSubRule(6);

			loop6:
			while (true) {
				int alt6=5;
				try { dbg.enterDecision(6, decisionCanBacktrack[6]);

				switch ( input.LA(1) ) {
				case PLUS:
					{
					alt6=1;
					}
					break;
				case MINUS:
					{
					alt6=2;
					}
					break;
				case SHIFTL:
					{
					alt6=3;
					}
					break;
				case SHIFTR:
					{
					alt6=4;
					}
					break;
				}
				} finally {dbg.exitDecision(6);}

				switch (alt6) {
				case 1 :
					dbg.enterAlt(1);

					// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:44:9: PLUS ^ multExpr
					{
					dbg.location(44,13);
					PLUS39=(Token)match(input,PLUS,FOLLOW_PLUS_in_expr307); 
					PLUS39_tree = (CommonTree)adaptor.create(PLUS39);
					root_0 = (CommonTree)adaptor.becomeRoot(PLUS39_tree, root_0);
					dbg.location(44,15);
					pushFollow(FOLLOW_multExpr_in_expr310);
					multExpr40=multExpr();
					state._fsp--;

					adaptor.addChild(root_0, multExpr40.getTree());

					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:45:9: MINUS ^ multExpr
					{
					dbg.location(45,14);
					MINUS41=(Token)match(input,MINUS,FOLLOW_MINUS_in_expr320); 
					MINUS41_tree = (CommonTree)adaptor.create(MINUS41);
					root_0 = (CommonTree)adaptor.becomeRoot(MINUS41_tree, root_0);
					dbg.location(45,16);
					pushFollow(FOLLOW_multExpr_in_expr323);
					multExpr42=multExpr();
					state._fsp--;

					adaptor.addChild(root_0, multExpr42.getTree());

					}
					break;
				case 3 :
					dbg.enterAlt(3);

					// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:46:9: SHIFTL ^ multExpr
					{
					dbg.location(46,15);
					SHIFTL43=(Token)match(input,SHIFTL,FOLLOW_SHIFTL_in_expr333); 
					SHIFTL43_tree = (CommonTree)adaptor.create(SHIFTL43);
					root_0 = (CommonTree)adaptor.becomeRoot(SHIFTL43_tree, root_0);
					dbg.location(46,17);
					pushFollow(FOLLOW_multExpr_in_expr336);
					multExpr44=multExpr();
					state._fsp--;

					adaptor.addChild(root_0, multExpr44.getTree());

					}
					break;
				case 4 :
					dbg.enterAlt(4);

					// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:47:9: SHIFTR ^ multExpr
					{
					dbg.location(47,15);
					SHIFTR45=(Token)match(input,SHIFTR,FOLLOW_SHIFTR_in_expr346); 
					SHIFTR45_tree = (CommonTree)adaptor.create(SHIFTR45);
					root_0 = (CommonTree)adaptor.becomeRoot(SHIFTR45_tree, root_0);
					dbg.location(47,17);
					pushFollow(FOLLOW_multExpr_in_expr349);
					multExpr46=multExpr();
					state._fsp--;

					adaptor.addChild(root_0, multExpr46.getTree());

					}
					break;

				default :
					break loop6;
				}
			}
			} finally {dbg.exitSubRule(6);}

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		dbg.location(49, 4);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "expr");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

		return retval;
	}
	// $ANTLR end "expr"


	public static class multExpr_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "multExpr"
	// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:51:1: multExpr : atom ( MUL ^ atom | DIV ^ atom | EXP ^ atom )* ;
	public final ExprParser.multExpr_return multExpr() throws RecognitionException {
		ExprParser.multExpr_return retval = new ExprParser.multExpr_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token MUL48=null;
		Token DIV50=null;
		Token EXP52=null;
		ParserRuleReturnScope atom47 =null;
		ParserRuleReturnScope atom49 =null;
		ParserRuleReturnScope atom51 =null;
		ParserRuleReturnScope atom53 =null;

		CommonTree MUL48_tree=null;
		CommonTree DIV50_tree=null;
		CommonTree EXP52_tree=null;

		try { dbg.enterRule(getGrammarFileName(), "multExpr");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(51, 0);

		try {
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:52:5: ( atom ( MUL ^ atom | DIV ^ atom | EXP ^ atom )* )
			dbg.enterAlt(1);

			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:52:7: atom ( MUL ^ atom | DIV ^ atom | EXP ^ atom )*
			{
			root_0 = (CommonTree)adaptor.nil();


			dbg.location(52,7);
			pushFollow(FOLLOW_atom_in_multExpr375);
			atom47=atom();
			state._fsp--;

			adaptor.addChild(root_0, atom47.getTree());
			dbg.location(53,7);
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:53:7: ( MUL ^ atom | DIV ^ atom | EXP ^ atom )*
			try { dbg.enterSubRule(7);

			loop7:
			while (true) {
				int alt7=4;
				try { dbg.enterDecision(7, decisionCanBacktrack[7]);

				switch ( input.LA(1) ) {
				case MUL:
					{
					alt7=1;
					}
					break;
				case DIV:
					{
					alt7=2;
					}
					break;
				case EXP:
					{
					alt7=3;
					}
					break;
				}
				} finally {dbg.exitDecision(7);}

				switch (alt7) {
				case 1 :
					dbg.enterAlt(1);

					// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:53:9: MUL ^ atom
					{
					dbg.location(53,12);
					MUL48=(Token)match(input,MUL,FOLLOW_MUL_in_multExpr385); 
					MUL48_tree = (CommonTree)adaptor.create(MUL48);
					root_0 = (CommonTree)adaptor.becomeRoot(MUL48_tree, root_0);
					dbg.location(53,14);
					pushFollow(FOLLOW_atom_in_multExpr388);
					atom49=atom();
					state._fsp--;

					adaptor.addChild(root_0, atom49.getTree());

					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:54:9: DIV ^ atom
					{
					dbg.location(54,12);
					DIV50=(Token)match(input,DIV,FOLLOW_DIV_in_multExpr398); 
					DIV50_tree = (CommonTree)adaptor.create(DIV50);
					root_0 = (CommonTree)adaptor.becomeRoot(DIV50_tree, root_0);
					dbg.location(54,14);
					pushFollow(FOLLOW_atom_in_multExpr401);
					atom51=atom();
					state._fsp--;

					adaptor.addChild(root_0, atom51.getTree());

					}
					break;
				case 3 :
					dbg.enterAlt(3);

					// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:55:9: EXP ^ atom
					{
					dbg.location(55,12);
					EXP52=(Token)match(input,EXP,FOLLOW_EXP_in_multExpr411); 
					EXP52_tree = (CommonTree)adaptor.create(EXP52);
					root_0 = (CommonTree)adaptor.becomeRoot(EXP52_tree, root_0);
					dbg.location(55,14);
					pushFollow(FOLLOW_atom_in_multExpr414);
					atom53=atom();
					state._fsp--;

					adaptor.addChild(root_0, atom53.getTree());

					}
					break;

				default :
					break loop7;
				}
			}
			} finally {dbg.exitSubRule(7);}

			}

			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		dbg.location(57, 4);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "multExpr");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

		return retval;
	}
	// $ANTLR end "multExpr"


	public static class atom_return extends ParserRuleReturnScope {
		CommonTree tree;
		@Override
		public CommonTree getTree() { return tree; }
	};


	// $ANTLR start "atom"
	// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:59:1: atom : ( INT | ID | LP ! expr RP !);
	public final ExprParser.atom_return atom() throws RecognitionException {
		ExprParser.atom_return retval = new ExprParser.atom_return();
		retval.start = input.LT(1);

		CommonTree root_0 = null;

		Token INT54=null;
		Token ID55=null;
		Token LP56=null;
		Token RP58=null;
		ParserRuleReturnScope expr57 =null;

		CommonTree INT54_tree=null;
		CommonTree ID55_tree=null;
		CommonTree LP56_tree=null;
		CommonTree RP58_tree=null;

		try { dbg.enterRule(getGrammarFileName(), "atom");
		if ( getRuleLevel()==0 ) {dbg.commence();}
		incRuleLevel();
		dbg.location(59, 0);

		try {
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:60:5: ( INT | ID | LP ! expr RP !)
			int alt8=3;
			try { dbg.enterDecision(8, decisionCanBacktrack[8]);

			switch ( input.LA(1) ) {
			case INT:
				{
				alt8=1;
				}
				break;
			case ID:
				{
				alt8=2;
				}
				break;
			case LP:
				{
				alt8=3;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 8, 0, input);
				dbg.recognitionException(nvae);
				throw nvae;
			}
			} finally {dbg.exitDecision(8);}

			switch (alt8) {
				case 1 :
					dbg.enterAlt(1);

					// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:60:7: INT
					{
					root_0 = (CommonTree)adaptor.nil();


					dbg.location(60,7);
					INT54=(Token)match(input,INT,FOLLOW_INT_in_atom440); 
					INT54_tree = (CommonTree)adaptor.create(INT54);
					adaptor.addChild(root_0, INT54_tree);

					}
					break;
				case 2 :
					dbg.enterAlt(2);

					// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:61:7: ID
					{
					root_0 = (CommonTree)adaptor.nil();


					dbg.location(61,7);
					ID55=(Token)match(input,ID,FOLLOW_ID_in_atom448); 
					ID55_tree = (CommonTree)adaptor.create(ID55);
					adaptor.addChild(root_0, ID55_tree);

					}
					break;
				case 3 :
					dbg.enterAlt(3);

					// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:62:7: LP ! expr RP !
					{
					root_0 = (CommonTree)adaptor.nil();


					dbg.location(62,9);
					LP56=(Token)match(input,LP,FOLLOW_LP_in_atom456); dbg.location(62,11);
					pushFollow(FOLLOW_expr_in_atom459);
					expr57=expr();
					state._fsp--;

					adaptor.addChild(root_0, expr57.getTree());
					dbg.location(62,18);
					RP58=(Token)match(input,RP,FOLLOW_RP_in_atom461); 
					}
					break;

			}
			retval.stop = input.LT(-1);

			retval.tree = (CommonTree)adaptor.rulePostProcessing(root_0);
			adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
			retval.tree = (CommonTree)adaptor.errorNode(input, retval.start, input.LT(-1), re);
		}
		finally {
			// do for sure before leaving
		}
		dbg.location(63, 4);

		}
		finally {
			dbg.exitRule(getGrammarFileName(), "atom");
			decRuleLevel();
			if ( getRuleLevel()==0 ) {dbg.terminate();}
		}

		return retval;
	}
	// $ANTLR end "atom"

	// Delegated rules



	public static final BitSet FOLLOW_stat_in_prog49 = new BitSet(new long[]{0x0000000000C24F00L});
	public static final BitSet FOLLOW_group_in_prog53 = new BitSet(new long[]{0x0000000000C24F00L});
	public static final BitSet FOLLOW_EOF_in_prog57 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LCB_in_group72 = new BitSet(new long[]{0x0000000000C64F00L});
	public static final BitSet FOLLOW_stat_in_group76 = new BitSet(new long[]{0x0000000000C64F00L});
	public static final BitSet FOLLOW_group_in_group80 = new BitSet(new long[]{0x0000000000C64F00L});
	public static final BitSet FOLLOW_RCB_in_group84 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_expr_in_stat97 = new BitSet(new long[]{0x0000000000004000L});
	public static final BitSet FOLLOW_NL_in_stat99 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_PRINT_in_stat111 = new BitSet(new long[]{0x0000000000000B00L});
	public static final BitSet FOLLOW_expr_in_stat113 = new BitSet(new long[]{0x0000000000004000L});
	public static final BitSet FOLLOW_NL_in_stat115 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_VAR_in_stat129 = new BitSet(new long[]{0x0000000000000100L});
	public static final BitSet FOLLOW_ID_in_stat131 = new BitSet(new long[]{0x0000000000010000L});
	public static final BitSet FOLLOW_PODST_in_stat133 = new BitSet(new long[]{0x0000000000000B00L});
	public static final BitSet FOLLOW_expr_in_stat135 = new BitSet(new long[]{0x0000000000004000L});
	public static final BitSet FOLLOW_NL_in_stat137 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_VAR_in_stat161 = new BitSet(new long[]{0x0000000000000100L});
	public static final BitSet FOLLOW_ID_in_stat163 = new BitSet(new long[]{0x0000000000004000L});
	public static final BitSet FOLLOW_NL_in_stat165 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_stat181 = new BitSet(new long[]{0x0000000000010000L});
	public static final BitSet FOLLOW_PODST_in_stat183 = new BitSet(new long[]{0x0000000000000B00L});
	public static final BitSet FOLLOW_expr_in_stat185 = new BitSet(new long[]{0x0000000000004000L});
	public static final BitSet FOLLOW_NL_in_stat187 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_function_in_stat205 = new BitSet(new long[]{0x0000000000004000L});
	public static final BitSet FOLLOW_NL_in_stat207 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_NL_in_stat223 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_TYPE_in_function246 = new BitSet(new long[]{0x0000000000000100L});
	public static final BitSet FOLLOW_ID_in_function248 = new BitSet(new long[]{0x0000000000000800L});
	public static final BitSet FOLLOW_LP_in_function250 = new BitSet(new long[]{0x0000000000480000L});
	public static final BitSet FOLLOW_params_in_function252 = new BitSet(new long[]{0x0000000000080000L});
	public static final BitSet FOLLOW_RP_in_function255 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_TYPE_in_params271 = new BitSet(new long[]{0x0000000000000100L});
	public static final BitSet FOLLOW_ID_in_params273 = new BitSet(new long[]{0x0000000000000012L});
	public static final BitSet FOLLOW_COMMA_in_params276 = new BitSet(new long[]{0x0000000000400000L});
	public static final BitSet FOLLOW_TYPE_in_params278 = new BitSet(new long[]{0x0000000000000100L});
	public static final BitSet FOLLOW_ID_in_params280 = new BitSet(new long[]{0x0000000000000012L});
	public static final BitSet FOLLOW_multExpr_in_expr297 = new BitSet(new long[]{0x0000000000309002L});
	public static final BitSet FOLLOW_PLUS_in_expr307 = new BitSet(new long[]{0x0000000000000B00L});
	public static final BitSet FOLLOW_multExpr_in_expr310 = new BitSet(new long[]{0x0000000000309002L});
	public static final BitSet FOLLOW_MINUS_in_expr320 = new BitSet(new long[]{0x0000000000000B00L});
	public static final BitSet FOLLOW_multExpr_in_expr323 = new BitSet(new long[]{0x0000000000309002L});
	public static final BitSet FOLLOW_SHIFTL_in_expr333 = new BitSet(new long[]{0x0000000000000B00L});
	public static final BitSet FOLLOW_multExpr_in_expr336 = new BitSet(new long[]{0x0000000000309002L});
	public static final BitSet FOLLOW_SHIFTR_in_expr346 = new BitSet(new long[]{0x0000000000000B00L});
	public static final BitSet FOLLOW_multExpr_in_expr349 = new BitSet(new long[]{0x0000000000309002L});
	public static final BitSet FOLLOW_atom_in_multExpr375 = new BitSet(new long[]{0x0000000000002062L});
	public static final BitSet FOLLOW_MUL_in_multExpr385 = new BitSet(new long[]{0x0000000000000B00L});
	public static final BitSet FOLLOW_atom_in_multExpr388 = new BitSet(new long[]{0x0000000000002062L});
	public static final BitSet FOLLOW_DIV_in_multExpr398 = new BitSet(new long[]{0x0000000000000B00L});
	public static final BitSet FOLLOW_atom_in_multExpr401 = new BitSet(new long[]{0x0000000000002062L});
	public static final BitSet FOLLOW_EXP_in_multExpr411 = new BitSet(new long[]{0x0000000000000B00L});
	public static final BitSet FOLLOW_atom_in_multExpr414 = new BitSet(new long[]{0x0000000000002062L});
	public static final BitSet FOLLOW_INT_in_atom440 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_atom448 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LP_in_atom456 = new BitSet(new long[]{0x0000000000000B00L});
	public static final BitSet FOLLOW_expr_in_atom459 = new BitSet(new long[]{0x0000000000080000L});
	public static final BitSet FOLLOW_RP_in_atom461 = new BitSet(new long[]{0x0000000000000002L});
}
