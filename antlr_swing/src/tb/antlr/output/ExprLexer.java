// $ANTLR 3.5.1 C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g 2022-03-15 19:14:24

package tb.antlr;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class ExprLexer extends Lexer {
	public static final int EOF=-1;
	public static final int COMMA=4;
	public static final int DIV=5;
	public static final int EXP=6;
	public static final int FUN=7;
	public static final int ID=8;
	public static final int INT=9;
	public static final int LCB=10;
	public static final int LP=11;
	public static final int MINUS=12;
	public static final int MUL=13;
	public static final int NL=14;
	public static final int PLUS=15;
	public static final int PODST=16;
	public static final int PRINT=17;
	public static final int RCB=18;
	public static final int RP=19;
	public static final int SHIFTL=20;
	public static final int SHIFTR=21;
	public static final int TYPE=22;
	public static final int VAR=23;
	public static final int WS=24;

	// delegates
	// delegators
	public Lexer[] getDelegates() {
		return new Lexer[] {};
	}

	public ExprLexer() {} 
	public ExprLexer(CharStream input) {
		this(input, new RecognizerSharedState());
	}
	public ExprLexer(CharStream input, RecognizerSharedState state) {
		super(input,state);
	}
	@Override public String getGrammarFileName() { return "C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g"; }

	// $ANTLR start "FUN"
	public final void mFUN() throws RecognitionException {
		try {
			int _type = FUN;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:65:4: ()
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:65:6: 
			{
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "FUN"

	// $ANTLR start "TYPE"
	public final void mTYPE() throws RecognitionException {
		try {
			int _type = TYPE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:68:3: ( 'int' | 'float' )
			int alt1=2;
			int LA1_0 = input.LA(1);
			if ( (LA1_0=='i') ) {
				alt1=1;
			}
			else if ( (LA1_0=='f') ) {
				alt1=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 1, 0, input);
				throw nvae;
			}

			switch (alt1) {
				case 1 :
					// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:69:3: 'int'
					{
					match("int"); 

					}
					break;
				case 2 :
					// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:70:5: 'float'
					{
					match("float"); 

					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "TYPE"

	// $ANTLR start "VAR"
	public final void mVAR() throws RecognitionException {
		try {
			int _type = VAR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:73:5: ( 'var' )
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:73:6: 'var'
			{
			match("var"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "VAR"

	// $ANTLR start "PRINT"
	public final void mPRINT() throws RecognitionException {
		try {
			int _type = PRINT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:75:6: ( 'print' )
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:75:8: 'print'
			{
			match("print"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PRINT"

	// $ANTLR start "ID"
	public final void mID() throws RecognitionException {
		try {
			int _type = ID;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:77:4: ( ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )* )
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:77:6: ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )*
			{
			if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:77:30: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )*
			loop2:
			while (true) {
				int alt2=2;
				int LA2_0 = input.LA(1);
				if ( ((LA2_0 >= '0' && LA2_0 <= '9')||(LA2_0 >= 'A' && LA2_0 <= 'Z')||LA2_0=='_'||(LA2_0 >= 'a' && LA2_0 <= 'z')) ) {
					alt2=1;
				}

				switch (alt2) {
				case 1 :
					// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop2;
				}
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "ID"

	// $ANTLR start "INT"
	public final void mINT() throws RecognitionException {
		try {
			int _type = INT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:79:5: ( ( '0' .. '9' )+ )
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:79:7: ( '0' .. '9' )+
			{
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:79:7: ( '0' .. '9' )+
			int cnt3=0;
			loop3:
			while (true) {
				int alt3=2;
				int LA3_0 = input.LA(1);
				if ( ((LA3_0 >= '0' && LA3_0 <= '9')) ) {
					alt3=1;
				}

				switch (alt3) {
				case 1 :
					// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt3 >= 1 ) break loop3;
					EarlyExitException eee = new EarlyExitException(3, input);
					throw eee;
				}
				cnt3++;
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "INT"

	// $ANTLR start "NL"
	public final void mNL() throws RecognitionException {
		try {
			int _type = NL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:81:4: ( ';' )
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:81:6: ';'
			{
			match(';'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "NL"

	// $ANTLR start "WS"
	public final void mWS() throws RecognitionException {
		try {
			int _type = WS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:83:4: ( ( ' ' | '\\t' | '\\r' | '\\n' )+ )
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:83:6: ( ' ' | '\\t' | '\\r' | '\\n' )+
			{
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:83:6: ( ' ' | '\\t' | '\\r' | '\\n' )+
			int cnt4=0;
			loop4:
			while (true) {
				int alt4=2;
				int LA4_0 = input.LA(1);
				if ( ((LA4_0 >= '\t' && LA4_0 <= '\n')||LA4_0=='\r'||LA4_0==' ') ) {
					alt4=1;
				}

				switch (alt4) {
				case 1 :
					// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:
					{
					if ( (input.LA(1) >= '\t' && input.LA(1) <= '\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt4 >= 1 ) break loop4;
					EarlyExitException eee = new EarlyExitException(4, input);
					throw eee;
				}
				cnt4++;
			}

			_channel = HIDDEN;
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "WS"

	// $ANTLR start "COMMA"
	public final void mCOMMA() throws RecognitionException {
		try {
			int _type = COMMA;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:85:7: ( ',' )
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:85:9: ','
			{
			match(','); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "COMMA"

	// $ANTLR start "LP"
	public final void mLP() throws RecognitionException {
		try {
			int _type = LP;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:87:4: ( '(' )
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:87:6: '('
			{
			match('('); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LP"

	// $ANTLR start "RP"
	public final void mRP() throws RecognitionException {
		try {
			int _type = RP;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:89:4: ( ')' )
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:89:6: ')'
			{
			match(')'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "RP"

	// $ANTLR start "PODST"
	public final void mPODST() throws RecognitionException {
		try {
			int _type = PODST;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:92:2: ( '=' )
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:92:4: '='
			{
			match('='); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PODST"

	// $ANTLR start "PLUS"
	public final void mPLUS() throws RecognitionException {
		try {
			int _type = PLUS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:96:2: ( '+' )
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:96:4: '+'
			{
			match('+'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "PLUS"

	// $ANTLR start "MINUS"
	public final void mMINUS() throws RecognitionException {
		try {
			int _type = MINUS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:100:2: ( '-' )
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:100:4: '-'
			{
			match('-'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "MINUS"

	// $ANTLR start "MUL"
	public final void mMUL() throws RecognitionException {
		try {
			int _type = MUL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:104:2: ( '*' )
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:104:4: '*'
			{
			match('*'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "MUL"

	// $ANTLR start "DIV"
	public final void mDIV() throws RecognitionException {
		try {
			int _type = DIV;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:108:2: ( '/' )
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:108:4: '/'
			{
			match('/'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "DIV"

	// $ANTLR start "EXP"
	public final void mEXP() throws RecognitionException {
		try {
			int _type = EXP;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:111:3: ( '^' )
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:111:5: '^'
			{
			match('^'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "EXP"

	// $ANTLR start "SHIFTL"
	public final void mSHIFTL() throws RecognitionException {
		try {
			int _type = SHIFTL;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:114:3: ( '<<' )
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:114:5: '<<'
			{
			match("<<"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "SHIFTL"

	// $ANTLR start "SHIFTR"
	public final void mSHIFTR() throws RecognitionException {
		try {
			int _type = SHIFTR;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:117:3: ( '>>' )
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:117:5: '>>'
			{
			match(">>"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "SHIFTR"

	// $ANTLR start "LCB"
	public final void mLCB() throws RecognitionException {
		try {
			int _type = LCB;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:120:5: ( '{' )
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:120:6: '{'
			{
			match('{'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LCB"

	// $ANTLR start "RCB"
	public final void mRCB() throws RecognitionException {
		try {
			int _type = RCB;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:122:5: ( '}' )
			// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:122:6: '}'
			{
			match('}'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "RCB"

	@Override
	public void mTokens() throws RecognitionException {
		// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:1:8: ( FUN | TYPE | VAR | PRINT | ID | INT | NL | WS | COMMA | LP | RP | PODST | PLUS | MINUS | MUL | DIV | EXP | SHIFTL | SHIFTR | LCB | RCB )
		int alt5=21;
		switch ( input.LA(1) ) {
		case 'i':
			{
			int LA5_2 = input.LA(2);
			if ( (LA5_2=='n') ) {
				int LA5_23 = input.LA(3);
				if ( (LA5_23=='t') ) {
					int LA5_27 = input.LA(4);
					if ( ((LA5_27 >= '0' && LA5_27 <= '9')||(LA5_27 >= 'A' && LA5_27 <= 'Z')||LA5_27=='_'||(LA5_27 >= 'a' && LA5_27 <= 'z')) ) {
						alt5=5;
					}

					else {
						alt5=2;
					}

				}

				else {
					alt5=5;
				}

			}

			else {
				alt5=5;
			}

			}
			break;
		case 'f':
			{
			int LA5_3 = input.LA(2);
			if ( (LA5_3=='l') ) {
				int LA5_24 = input.LA(3);
				if ( (LA5_24=='o') ) {
					int LA5_28 = input.LA(4);
					if ( (LA5_28=='a') ) {
						int LA5_32 = input.LA(5);
						if ( (LA5_32=='t') ) {
							int LA5_35 = input.LA(6);
							if ( ((LA5_35 >= '0' && LA5_35 <= '9')||(LA5_35 >= 'A' && LA5_35 <= 'Z')||LA5_35=='_'||(LA5_35 >= 'a' && LA5_35 <= 'z')) ) {
								alt5=5;
							}

							else {
								alt5=2;
							}

						}

						else {
							alt5=5;
						}

					}

					else {
						alt5=5;
					}

				}

				else {
					alt5=5;
				}

			}

			else {
				alt5=5;
			}

			}
			break;
		case 'v':
			{
			int LA5_4 = input.LA(2);
			if ( (LA5_4=='a') ) {
				int LA5_25 = input.LA(3);
				if ( (LA5_25=='r') ) {
					int LA5_29 = input.LA(4);
					if ( ((LA5_29 >= '0' && LA5_29 <= '9')||(LA5_29 >= 'A' && LA5_29 <= 'Z')||LA5_29=='_'||(LA5_29 >= 'a' && LA5_29 <= 'z')) ) {
						alt5=5;
					}

					else {
						alt5=3;
					}

				}

				else {
					alt5=5;
				}

			}

			else {
				alt5=5;
			}

			}
			break;
		case 'p':
			{
			int LA5_5 = input.LA(2);
			if ( (LA5_5=='r') ) {
				int LA5_26 = input.LA(3);
				if ( (LA5_26=='i') ) {
					int LA5_30 = input.LA(4);
					if ( (LA5_30=='n') ) {
						int LA5_34 = input.LA(5);
						if ( (LA5_34=='t') ) {
							int LA5_36 = input.LA(6);
							if ( ((LA5_36 >= '0' && LA5_36 <= '9')||(LA5_36 >= 'A' && LA5_36 <= 'Z')||LA5_36=='_'||(LA5_36 >= 'a' && LA5_36 <= 'z')) ) {
								alt5=5;
							}

							else {
								alt5=4;
							}

						}

						else {
							alt5=5;
						}

					}

					else {
						alt5=5;
					}

				}

				else {
					alt5=5;
				}

			}

			else {
				alt5=5;
			}

			}
			break;
		case 'A':
		case 'B':
		case 'C':
		case 'D':
		case 'E':
		case 'F':
		case 'G':
		case 'H':
		case 'I':
		case 'J':
		case 'K':
		case 'L':
		case 'M':
		case 'N':
		case 'O':
		case 'P':
		case 'Q':
		case 'R':
		case 'S':
		case 'T':
		case 'U':
		case 'V':
		case 'W':
		case 'X':
		case 'Y':
		case 'Z':
		case '_':
		case 'a':
		case 'b':
		case 'c':
		case 'd':
		case 'e':
		case 'g':
		case 'h':
		case 'j':
		case 'k':
		case 'l':
		case 'm':
		case 'n':
		case 'o':
		case 'q':
		case 'r':
		case 's':
		case 't':
		case 'u':
		case 'w':
		case 'x':
		case 'y':
		case 'z':
			{
			alt5=5;
			}
			break;
		case '0':
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			{
			alt5=6;
			}
			break;
		case ';':
			{
			alt5=7;
			}
			break;
		case '\t':
		case '\n':
		case '\r':
		case ' ':
			{
			alt5=8;
			}
			break;
		case ',':
			{
			alt5=9;
			}
			break;
		case '(':
			{
			alt5=10;
			}
			break;
		case ')':
			{
			alt5=11;
			}
			break;
		case '=':
			{
			alt5=12;
			}
			break;
		case '+':
			{
			alt5=13;
			}
			break;
		case '-':
			{
			alt5=14;
			}
			break;
		case '*':
			{
			alt5=15;
			}
			break;
		case '/':
			{
			alt5=16;
			}
			break;
		case '^':
			{
			alt5=17;
			}
			break;
		case '<':
			{
			alt5=18;
			}
			break;
		case '>':
			{
			alt5=19;
			}
			break;
		case '{':
			{
			alt5=20;
			}
			break;
		case '}':
			{
			alt5=21;
			}
			break;
		default:
			alt5=1;
		}
		switch (alt5) {
			case 1 :
				// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:1:10: FUN
				{
				mFUN(); 

				}
				break;
			case 2 :
				// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:1:14: TYPE
				{
				mTYPE(); 

				}
				break;
			case 3 :
				// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:1:19: VAR
				{
				mVAR(); 

				}
				break;
			case 4 :
				// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:1:23: PRINT
				{
				mPRINT(); 

				}
				break;
			case 5 :
				// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:1:29: ID
				{
				mID(); 

				}
				break;
			case 6 :
				// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:1:32: INT
				{
				mINT(); 

				}
				break;
			case 7 :
				// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:1:36: NL
				{
				mNL(); 

				}
				break;
			case 8 :
				// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:1:39: WS
				{
				mWS(); 

				}
				break;
			case 9 :
				// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:1:42: COMMA
				{
				mCOMMA(); 

				}
				break;
			case 10 :
				// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:1:48: LP
				{
				mLP(); 

				}
				break;
			case 11 :
				// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:1:51: RP
				{
				mRP(); 

				}
				break;
			case 12 :
				// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:1:54: PODST
				{
				mPODST(); 

				}
				break;
			case 13 :
				// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:1:60: PLUS
				{
				mPLUS(); 

				}
				break;
			case 14 :
				// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:1:65: MINUS
				{
				mMINUS(); 

				}
				break;
			case 15 :
				// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:1:71: MUL
				{
				mMUL(); 

				}
				break;
			case 16 :
				// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:1:75: DIV
				{
				mDIV(); 

				}
				break;
			case 17 :
				// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:1:79: EXP
				{
				mEXP(); 

				}
				break;
			case 18 :
				// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:1:83: SHIFTL
				{
				mSHIFTL(); 

				}
				break;
			case 19 :
				// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:1:90: SHIFTR
				{
				mSHIFTR(); 

				}
				break;
			case 20 :
				// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:1:97: LCB
				{
				mLCB(); 

				}
				break;
			case 21 :
				// C:\\Users\\Emilia\\git\\antlr_swing\\antlr_swing\\src\\tb\\antlr\\Expr.g:1:101: RCB
				{
				mRCB(); 

				}
				break;

		}
	}



}
