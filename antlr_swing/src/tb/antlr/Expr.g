grammar Expr;



options {
  output=AST;
  ASTLabelType=CommonTree;
}

tokens{
FUN;
PARAM;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (stat | group)+ EOF!;
    
group
  : LCB^ (stat | group)* RCB!;

stat
    : expr NL -> expr
    | PRINT expr NL -> PRINT expr
    | VAR ID PODST expr NL -> ^(VAR ID) ^(PODST ID expr)
    | VAR ID NL -> ^(VAR ID)
    | ID PODST expr NL -> ^(PODST ID expr)
    | function NL -> function
    | ifstate NL -> ifstate
    | NL ->
    ;
ifstate
    : IF^ LP! expr OPERATOR expr  RP! THEN! expr (ELSE! expr)?
    ;

function
  :
  TYPE ID LP params? RP -> ^(FUN TYPE ID params?)
  ;

params
  :
  TYPE ID (COMMA TYPE ID)* -> ^(PARAM ^(TYPE ID)*)
  ;

expr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      | SHIFTL^ multExpr
      | SHIFTR^ multExpr
      )*
    ;

multExpr
    : atom
      ( MUL^ atom
      | DIV^ atom
      | EXP^ atom
      )*
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;

TYPE
  :
  'int'
  | 'float'
  ;
IF : 'if';

ELSE : 'else';

THEN : 'then';

VAR :'var';

PRINT: 'print';

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : ';' ;

WS : (' ' | '\t' | '\r'| '\n')+ {$channel = HIDDEN;} ;

COMMA : ',';

LP :	'(';

RP :	')';

PODST
	:	'='
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

MUL
	:	'*'
	;

DIV
	:	'/'
	;
EXP
  : '^'
  ;
SHIFTL
  : '<<'
  ;
SHIFTR
  : '>>'
  ;
  
LCB :'{';

RCB :'}';

OPERATOR : '==' | '!=' | '>' | '<';

  
